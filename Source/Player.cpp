#include "main.h"

//コンストラクタ
Player::Player(Game& C_Game,MainGame& C_MainGame,Controller& C_Controller):
game(C_Game), maingame(C_MainGame),controller(C_Controller){

}

//デストラクタ
Player::~Player(){

}

//初期化関数
void Player::Inialize(){
}

//武器性能決定関数
void Player::Set_Weapon_Level(){
	this->setter_num = WEAPON_NUM[this->game.Get_SaveData().weapon_num_level];
	this->max_angle = WEAPON_ANGLE[this->game.Get_SaveData().weapon_angle_level];
	this->weapon_time = this->game.Get_SaveData().weapon_time_level * 30 + 120;
}

//武器設定
void Player::Weapon_Set(){
	if (this->controller.Get_Mouse().input[MOUSE_LEFT] == 1){
		this->root_position.Set((float)this->controller.Get_Mouse().x, (float)this->controller.Get_Mouse().y);
		//武器の性能決定
		this->Set_Weapon_Level();
	}

	//発射処理
	//前フレームまで左クリックが押されていて,今のフレームで離されたら発射
	if (this->controller.Get_Pre_Mouse().input[MOUSE_LEFT] > 0 &&
		this->controller.Get_Mouse().input[MOUSE_LEFT] == 0){
		//発射
		//効果音発射
		this->game.Get_Sound().Set_Sound(SOUND_WEAPON_SHOOT, TRUE);
		for (int i = 0; i < this->setter_num; i++){
			Vector2D vec((float)this->controller.Get_Pre_Mouse().x, (float)this->controller.Get_Mouse().y);
			float length = Vector2D::Two_Length(this->root_position, vec);
			length /= 10;
			if (length  < WEAPON_SPEED_UNDER)length = WEAPON_SPEED_UNDER;
			if (length  > WEAPON_SPEED_MAX)length = WEAPON_SPEED_MAX;
			Vector2D set_vec = this->root_position - vec;
			this->maingame.Get_Weapon_List().push_back(std::shared_ptr<Weapon>(new Weapon(
				this->game, this->maingame, this->root_position, length,
				set_vec.Get_Angle()-this->max_angle/2+this->max_angle/this->setter_num*i,this->weapon_time)));
		}
	}
}

//描画関数
void Player::Draw(){
	//左クリックが押されていたら
	if (this->controller.Get_Mouse().input[MOUSE_LEFT] > 0){
		for (int i = 0; i < this->setter_num; i++){
			//押し始めた場所に銃を設置
			DrawRotaGraph2F(
				this->root_position.Get_X(), this->root_position.Get_Y(),
				15.0f, 110.0f, 1.0f,
				atan2f(this->controller.Get_Mouse().y - this->root_position.Get_Y(),
				this->controller.Get_Mouse().x - this->root_position.Get_X()) - PI / 2 -
				this->max_angle / 2 + this->max_angle / this->setter_num*i,
				this->game.Get_Sprite().Get_Image(IMAGE_WEAPON_SETTER), TRUE);
		}
	}
}

//アップデート関数
void Player::UpDate(){
	DrawCircle(this->controller.Get_Mouse().x, this->controller.Get_Mouse().y, 10, 0x00ff00, FALSE);//マウスカーソルの位置に円を描画
	this->Weapon_Set();//武器の登録
	this->Draw();//武器の描画
}
