#include "main.h"

//コンストラクタ
Target::Target(Game& C_Game,MainGame& C_MainGame,Position2D pos,float size) :
game(C_Game), maingame(C_MainGame){
	this->initialize();
	this->position = pos;
	this->size_rate = size;
}

//デストラクタ
Target::~Target(){

}

//座標の取得
Position2D& Target::Get_Position(){
	return this->position;
}

//ベクトルの取得
Vector2D& Target::Get_Vector(){
	return this->velocity;
}

//大きさの取得
float& Target::Get_Size_Rate(){
	return this->size_rate;
}

//所属番号の取得
int Target::Get_Group(){
	return this->group;
}

//初期化関数
void Target::initialize(){
	float angle = PI / 180 * GetRand(360);
	this->velocity.Set_Angle(5.0f, angle);
	this->angle = angle;
	this->state = 0;
	this->count = 0;
	this->group = GetRand(GROUP_MAX - 1);
}

//移動関数
void Target::Move(){
	//群れに合わせてベクトルを変形

	//群れの移動方向から変形
	Vector2D vec = this->maingame.Get_Leader_Velocity()[this->group];
	vec.Norm();
	vec.Mul(0.2f);//比重

	//群れの位置から変形
	Vector2D vec2 = this->maingame.Get_Leader_Position()[this->group] - this->position;
	vec2.Norm();
	vec2.Mul(0.01f);//比重

	//現在の移動う速度を取得
	float length = this->velocity.Get_Length();
	//変形用のベクトルを加算
	this->velocity.Add(vec);
	this->velocity.Add(vec2);
	//ベクトルを一度正規化
	this->velocity.Norm();
	//元の速度に戻す
	this->velocity.Mul(length);
	//移動方向の更新
	this->angle = this->velocity.Get_Angle();
	//座標に加算
	this->position.Add(this->velocity);

	//画面外に出る場合
	//反射する
	if (this->position.Get_X() < 0){
		this->position.Set_X(0);
		this->velocity.Set_X(this->velocity.Get_X()*(-1));
		this->angle = this->velocity.Get_Angle();
	}
	if (this->position.Get_X()  >FIELD_X){
		this->position.Set_X(FIELD_X);
		this->velocity.Set_X(this->velocity.Get_X()*(-1));
		this->angle = this->velocity.Get_Angle();
	}
	if (this->position.Get_Y() < 0){
		this->position.Set_Y(0);
		this->velocity.Set_Y(this->velocity.Get_Y()*(-1));
		this->angle = this->velocity.Get_Angle();
	}
	if (this->position.Get_Y() > FIELD_Y){
		this->position.Set_Y(FIELD_Y);
		this->velocity.Set_Y(this->velocity.Get_Y()*(-1));
		this->angle = this->velocity.Get_Angle();
	}
}

//当たり判定
void Target::Collision(){
	for (auto We : this->maingame.Get_Weapon_List()){
		if (Vector2D::Two_Length(this->position, We.get()->Get_Position()) < collision*this->size_rate/100+WEAPON_COLLISION){
			//当たった場合
			this->state = GetRand(1)+2;//2つの表情のどちらかになる
			this->count = 0;
		}
	}
}

//描画関数
void Target::Draw(){
	DrawRotaGraphF(
		this->position.Get_X(), this->position.Get_Y(),
		this->size_rate/100, this->angle + PI / 2, this->game.Get_Sprite().Get_Image(IMAGE_TARGET01+this->state), TRUE);
}

//アップデート関数
BOOL Target::Update(){
	this->count++;//カウンター可算
	if (this->state == 1){
		this->Collision();
	}
	if (this->count == 30){
		if (this->state == 0)this->state = 1;
		if (this->state == 2 || this->state==3)return FALSE;
	}
	this->Move();//移動制御
	//this->Draw();

	return TRUE;
	
}