//定数ファイル

const int NOW_VERSION = 101;//現在のバージョン
//ウィンドウサイズ
const int WINDOW_X = 1280;
const int WINDOW_Y = 720;

//円周率
const float PI = 3.141592653589f;
const float PI2 = PI * 2;

const int FIRST_WINDOW_EXPAND = 100;//ウィンドウの拡大率
const int FIRST_TARGET_DRAW_MAX = 1000;//的の描画最大数(変更可能)
const int FIRST_TARGET_APPEAR_MAX = 3000;//的の出現最大数

//Fps.h
const int FPS = 60;

//Sprite.h
const int IMAGE_MAX = 11;
enum IMAGE{
	IMAGE_BACK01,//壁紙
	IMAGE_BACK02,
	IMAGE_BACK03,
	IMAGE_BACK04,
	IMAGE_ROOM,//部屋
	IMAGE_TARGET01,//的登場時
	IMAGE_TARGET02,//的常駐時
	IMAGE_TARGET03,//的撃破時
	IMAGE_TARGET04,//的撃破時その2
	IMAGE_WEAPON_SETTER,//武器発射台
	IMAGE_WEAPON,//武器
};
const int SOUND_MAX = 4;
enum SOUND{
	SOUND_WEAPON_SHOOT,//武器発射音
	SOUND_LEVELUP,//レベルアップ音
	SOUND_CURSOR_SELECT,//カーソルセレクト音
	SOUND_TARGET_ENCOUNT,//的発生音
};

//Load.h

//MainGame.h//
//フィールドサイズ
const int FIELD_X = 1280;
const int FIELD_Y = 720;

const int TARGET_DEFENCE_MAX = 10;//プロ生ちゃんの耐久力上限
//レベル別プロ生ちゃんの耐久力
const float TARGET_DEFENCE[TARGET_DEFENCE_MAX] = {
	50.0f,
	48.0f,
	45.0f,
	40.0f,
	38.0f,
	35.0f,
	30.0f,
	27.0f,
	25.0f,
	10.0f,
};
//プロ生ちゃん耐久力のレベルアップコスト
const int TARGET_DEFENCE_COST[TARGET_DEFENCE_MAX-1] = {
	8000,
	10000,
	50000,
	75000,
	100000,
	150000,
	200000,
	500000,
	1000000,
};

const int TARGET_COPY_LEVEL_MAX = 7;//的の増殖レベル最大数
//レベル別プロ生ちゃん増殖力
const int TARGET_COPY_NUM[TARGET_COPY_LEVEL_MAX] = {
	2, 3, 4, 5, 6, 7, 10,
};
//プロ生ちゃん増殖力のレベルアップコスト
const int TARGET_COPY_COST[TARGET_COPY_LEVEL_MAX - 1] = {
	10000,
	100000,
	500000,
	1000000,
	5000000,
	10000000,
};

//Controller.h//
const int MOUSE_INPUT_MAX = 3;//マウスボタンの最大数
enum MOUSE_INPUT{
	MOUSE_LEFT,//左クリック
	MOUSE_RIGHT,//右クリック
	MOUSE_WHEEL,//ホイール
};

//Target.h
const float collision = 80.0f;//プロ生ちゃんの初期当たり判定の大きさ

//player.h//
const float WEAPON_SPEED_UNDER = 5.0f;//武器速度下限
const float WEAPON_SPEED_MAX = 20.0f;//武器速度上限
const int WEAPON_NUM_LEVEL_MAX = 15;//武器同時発射数の最大レベル
//レベル別武器の同時発射数
const int WEAPON_NUM[WEAPON_NUM_LEVEL_MAX] = {
	1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
	15, 24, 30, 60,120,
};
//武器の同時発射数レベルアップコスト
const int WEAPON_NUM_COST[WEAPON_NUM_LEVEL_MAX-1] = {
	3000,
	10000,
	30000,
	50000,
	70000,
	100000,
	150000,
	200000,
	250000,
	500000,
	1000000,
	2000000,
	5000000,
	10000000,
};

const int WEAPON_ANGLE_LEVEL_MAX = 10;//武器の発射角度の最大レベル
//レベル別武器の発射角度
const float WEAPON_ANGLE[WEAPON_ANGLE_LEVEL_MAX] = {
	0,//0°
	PI / 12,//15°
	PI / 6,//30°
	PI / 4,//45°
	PI / 3,//60°
	PI / 2,//90°
	PI / 3 * 2,//120
	PI / 6 * 5,//150°
	PI,//180°
	PI / 3 * 4,//240°
};
//武器の発射角度レベルアップコスト
const int WEAPON_ANGLE_COST[WEAPON_ANGLE_LEVEL_MAX-1] = {
	3000,
	15000,
	45000,
	100000,
	200000,
	1000000,
	2000000,
	5000000,
	10000000,
};

const int WEAPON_TIME_LEVEL_MAX = 100;//武器の存続時間最大レベル

//Weapon.h//
const float WEAPON_COLLISION=10.0F;//武器の当たり判定サイズ

//Target.h//
const int GROUP_MAX = 1;//群れ最大数