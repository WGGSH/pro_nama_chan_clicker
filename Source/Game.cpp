#include "main.h"

Game::Game(){
	//セーブデータクラス
	this->savedata = new SaveData;
	this->optiondata = new OptionData;
	this->save = new Save(*this);
	this->load = new Load(*this);

	//クラス生成
	this->controller = new Controller(*this);
	this->sound = new Sound(*this);
	this->maingame = new MainGame(*this);
	this->fps = new Fps(*this);

	//DxLib初期化準備
	SetWindowText("プロ生ちゃんくりっか〜　Ver1.01");//タイトル文字
	SetAlwaysRunFlag(TRUE);//ウィンドウが非アクティブでも処理を行う
	SetWindowSizeExtendRate(this->optiondata->window_expand / 100.0);//拡大率設定
	SetOutApplicationLogValidFlag(FALSE);//ログ出力を行わない
	SetWindowPosition(0, 0);//ウィンドウ位置設定
	SetWindowSizeChangeEnableFlag(TRUE);//ウィンドウの拡大縮小を許可
	SetGraphMode(WINDOW_X, WINDOW_Y, 32);//画面サイズ設定
	ChangeWindowMode(TRUE);//ウィンドウモードで起動
	SetWindowIconID(IDI_ICON1);//アイコンの設定
	//DxLib初期化処理
	DxLib_Init();
	//DxLib初期化後初期設定
	SetDrawScreen(DX_SCREEN_BACK);

	//スプライトクラスの初期化
	//DxLib_Init後に行わなければ画像/音声の読み込みができない
	this->sprite = new Sprite(*this);
}

//デストラクタ
Game::~Game(){
	DxLib_End();//DxLibの終了処理
	delete this->controller;
	delete this->sound;
	delete this->maingame;
	delete this->sprite;
	delete this->save;
	delete this->load;
	delete this->fps;
	delete this->savedata;
	delete this->optiondata;
}

//MainGameの取得
MainGame& Game::Get_MainGame(){
	return *this->maingame;
}

//Controllerの取得
Controller& Game::Get_Controller(){
	return *this->controller;
}

//Spriteの取得
Sprite& Game::Get_Sprite(){
	return *this->sprite;
}

//Saveの取得
Save& Game::Get_Save(){
	return *this->save;
}

//Loadの取得
Load& Game::Get_Load(){
	return *this->load;
}

//SaveDataの取得
SaveData& Game::Get_SaveData(){
	return *this->savedata;
}

//OptionDataの取得
OptionData& Game::Get_OptionData(){
	return *this->optiondata;
}

//Soundの取得
Sound& Game::Get_Sound(){
	return *this->sound;
}

//SSを撮影するかの判定を設定
void Game::Set_SS_Flag(BOOL flag){
	this->ss_flag = flag;
}

//ゲームを終了するかの判定を設定
void Game::Set_Quit_Flag(BOOL flag){
	this->quit_flag = flag;
}

//初期化関数
void Game::Initialize(){
	this->controller->Initialize();
	this->maingame->Initialize();

	this->ss_flag = FALSE;
}

//更新関数
void Game::Update(){
	this->Initialize();//初期化
	while (ProcessMessage() == 0){//プロセス処理
		//描画内容を消去
		if (ClearDrawScreen() == -1)break;
		this->ss_flag = FALSE;
		this->controller->Update();//マウス,キー状態の取得

		this->maingame->update();//ゲームメイン処理

		//SSを撮影するフラグが立っていれば
		if (this->ss_flag == TRUE){
			//撮影する
			SaveDrawScreen(0, 0, WINDOW_X, WINDOW_Y, "SS.png");//SSを撮影する
		}

		this->sound->Update();//効果音再生処理
		this->fps->Update();//FPS制御

		//描画反映処理
		if (ScreenFlip() == -1)break;
		//終了処理
		if (this->quit_flag == TRUE)break;
	}
}