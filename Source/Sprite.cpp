#include "main.h"

//コンストラクタ
Sprite::Sprite(Game& C_Game) :
game(C_Game){
	this->Initialize();
}

//デストラクタ
Sprite::~Sprite(){

}

//初期化関数
void Sprite::Initialize(){
	//画像の読み込み

	//音声の読み込み
}

//返却関数
int& Sprite::Get_Image(int number){
	return this->image[number];
}

//返却関数
int& Sprite::Get_Sound(int number){
	return this->sound[number];
}