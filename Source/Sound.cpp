#include "main.h"

//コンストラクタ
Sound::Sound(Game& C_game) :
game(C_game){

}

//デストラクタ
Sound::~Sound(){
}

//音声再生フラグのセット関数
void Sound::Set_Sound(int number, BOOL flag){
	this->sound_handle[number] = flag;
}

//初期化関数
void Sound::Initialize(){
	for (int i = 0; i < SOUND_MAX; i++){
		this->sound_handle[i] = FALSE;
	}
}

//アップデート関数
void Sound::Update(){
	for (int i = 0; i < SOUND_MAX; i++){
		if (this->sound_handle[i] == TRUE){
			//再生フラグが立っているとその音声を鳴らす
			PlaySoundMem(this->game.Get_Sprite().Get_Sound(i), DX_PLAYTYPE_BACK);
			this->sound_handle[i] = FALSE;//フラグを切る
		}
	}
}