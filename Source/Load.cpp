#include "main.h"

//コンストラクタ
Load::Load(Game& C_Game) :
game(C_Game){
	this->Initialize();
}

//デストラクタ
Load::~Load(){

}

//初期化処理
void Load::Initialize(){
	FILE *load_file;//ファイルの読み込み用ポインタ
	errno_t error;//エラーフラグ
	BOOL ver_error;//バージョンが違った場合
	int gold;//所持金

	//セーブデータの読み込み
	if (error = fopen_s(&load_file, "data/save.dat", "rb") != 0){
		//ファイル読み込みに失敗した場合
		//バージョン登録
		this->game.Get_SaveData().version = NOW_VERSION;
		this->game.Get_SaveData().exp = 0;
		this->game.Get_SaveData().play_count = 0;
		this->game.Get_SaveData().target_break = 0;
		this->game.Get_SaveData().weapon_angle_level = 0;
		this->game.Get_SaveData().weapon_num_level = 0;
		this->game.Get_SaveData().weapon_time_level = 0;
		this->game.Get_SaveData().target_defence_level = 0;
		this->game.Get_SaveData().target_copy_level = 0;
	}
	else{

		int ver;
		fread(&ver, sizeof(int), 1, load_file);
		rewind(load_file);

		if (ver == NOW_VERSION){
			//現在バージョンのセーブデータを読み込んだ場合

			//fread(&gold, sizeof(int), 1, load_file);
			fread(&this->game.Get_SaveData(), sizeof(SaveData), 1, load_file);
		}
		else{
			//違うバージョンのセーブデータだった場合
			//バージョン登録
			this->game.Get_SaveData().version = NOW_VERSION;
			this->game.Get_SaveData().exp = 0;
			this->game.Get_SaveData().play_count = 0;
			this->game.Get_SaveData().target_break = 0;
			this->game.Get_SaveData().weapon_angle_level = 0;
			this->game.Get_SaveData().weapon_num_level = 0;
			this->game.Get_SaveData().weapon_time_level = 0;
		}
		fclose(load_file);
	}

	//オプションデータの読み込み
	if (error = fopen_s(&load_file, "data/option.dat", "rb") != 0){
		//ファイル読み込みに失敗した場合
		this->game.Get_OptionData().window_expand = 100;
		this->game.Get_OptionData().target_draw_max = 2000;
		this->game.Get_OptionData().target_appear_max = 6000;
	}
	else{
		//データ読み込み
		fread(&this->game.Get_OptionData(), sizeof(OptionData), 1, load_file);
	}
}