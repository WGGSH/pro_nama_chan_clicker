#include "main.h"

//コンストラクタ
Fps::Fps(Game& C_Game) :game(C_Game){
	//イニシャライズ
	//Gameクラスへの参照を渡す
	this->count0t = 0;
	this->fps_count = 0;
}

//デストラクタ
Fps::~Fps(){

}

//待機関数
void Fps::Wait(){
	int term, i, gnt;
	static int t = 0;
	if (this->fps_count == 0){//60フレームの1回目なら
		if (t == 0)//完全に最初ならまたない
			term = 0;
		else//前回記録した時間を元に計算
			term = this->count0t + 1000 - GetNowCount();
	}
	else    //待つべき時間=現在あるべき時刻-現在の時刻
		term = (int)(this->count0t + this->fps_count*(1000.0 / FPS)) - GetNowCount();

	if (term>0)//待つべき時間だけ待つ
		Sleep(term);

	gnt = GetNowCount();

	if (this->fps_count == 0){//60フレームに1度基準を作る
		this->count0t = gnt;
		//FPSが下がっていたら
		if (1000.0/this->average < FPS*0.8f){
			//40%のプロ生ちゃんを消去
			int num = (int)(this->game.Get_MainGame().Get_Target_List().size()*0.4f);
			for (int i = 0; i < num; i++){
				this->game.Get_MainGame().Get_Target_List().pop_back();
			}
		}
	}
	this->f[this->fps_count] = gnt - t;//１周した時間を記録
	t = gnt;
	//平均計算
	if (this->fps_count == FPS - 1){
		this->average = 0;
		for (i = 0; i<FPS; i++)
			this->average += this->f[i];
		this->average /= FPS;
	}
	this->fps_count = (++(this->fps_count)) % FPS;
}

//描画関数
//実装予定なし
void Fps::Draw(){
	return;
}

//アップデート関数
void Fps::Update(){
	this->Wait();
}