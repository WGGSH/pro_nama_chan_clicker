class Player{
private:
	Game& game;
	MainGame& maingame;
	Controller& controller;
	Position2D root_position;//発生源

	int setter_num;//同時発射数
	float max_angle;//最大発射角度
	int weapon_time;//武器の存続時間

	void Set_Weapon_Level();//武器の性能決定関数
	void Weapon_Set();//武器発射関数
	void Draw();//描画関数
public:
	Player(Game& C_Game, MainGame& C_MainGame,Controller& C_Controller);//コンストラクタ
	~Player();//デストラクタ

	void Inialize();//初期化関数
	void UpDate();//アップデート関数
};