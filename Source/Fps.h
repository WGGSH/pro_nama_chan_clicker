//Fpsクラス
//動作速度を制御する

class Fps{
private:
	Game& game;//Gameクラスの参照

	int fps_count;
	int count0t;
	int f[FPS];
	float average;

	void Wait();//待機関数
	void Draw();//描画関数
public:
	Fps(Game& game);//コンストラクタ
	~Fps();//デストラクタ

	void Update();//アップデート
};