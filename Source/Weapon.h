class Weapon{
private:
	Game& game;
	MainGame& maingame;

	Position2D position;//座標
	Vector2D velocity;//移動量
	float angle;//進行方向
	int count;//カウンタ

	void Draw();//描画関数
public:
	Weapon(Game& C_Game,MainGame& C_MainMame,Position2D& pos ,float speed,float angle,int count);//コンストラクタ
	~Weapon();//デストラクタ

	//void Initialize();
	BOOL Update();//更新関数

	Position2D& Get_Position();//座標の取得
};