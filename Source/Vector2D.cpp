#include "main.h"

//コンストラクタ1
//全要素を0で初期化
Vector2D::Vector2D(){
	this->x = 0;
	this->y = 0;
}

//コンストラクタ2
//指定した値で要素を初期化
Vector2D::Vector2D(float vx, float vy){
	this->x = vx;
	this->y = vy;
}

//デストラクタ
Vector2D::~Vector2D(){

}

//値の代入関数1
//指定した値を各要素に代入
void Vector2D::Set(float vx, float vy){
	this->x = vx;
	this->y = vy;
}

//値の代入関数2
//長さ,角度からベクトルを生成
void Vector2D::Set_Angle(float length, float angle){
	this->x = length*cosf(angle);
	this->y = length*sinf(angle);
}

//X成分の代入関数
void Vector2D::Set_X(float value){
	this->x = value;
}

//Y成分の代入関数
void Vector2D::Set_Y(float value){
	this->y = value;
}

//X成分の取得関数
float Vector2D::Get_X(){
	return this->x;
}

//Y成分の取得関数
float Vector2D::Get_Y(){
	return this->y;
}

//加算関数
//ベクトルを引数に取る
void Vector2D::Add(Vector2D& vec){
	this->x += vec.x;
	this->y += vec.y;
}

//加算関数
//2つの実数を引数に取る
void Vector2D::Add(float vx, float vy){
	this->x += vx;
	this->y += vy;
}

//減算関数
//ベクトルを引数に取る
void Vector2D::Sub(Vector2D& vec){
	this->x -= vec.x;
	this->y -= vec.y;
}

//減算関数
//2つの実数を引数に取る
void Vector2D::Sub(float vx, float vy){
	this->x -= vx;
	this->y -= vy;
}

//乗算関数
//実数を引数に取り,両成分に乗算する
void Vector2D::Mul(float mul){
	this->x *= mul;
	this->y *= mul;
}

//除算関数
//実数を引数に取り,両成分に除算する
//引数が0の場合は処理をしない
void Vector2D::Div(float div){
	if (div == 0)return;
	this->x /= div;
	this->y /= div;
}

//ベクトルの正規化
void Vector2D::Norm(){
	float length = this->Get_Length();
	this->Div(length);
}

//ベクトルを回転させる
void Vector2D::Rotate(float angle){
	Vector2D R;
	R.Set_X(this->x*cosf(angle) - this->y*sinf(angle));
	R.Set_Y(this->x*sinf(angle) + this->y*cosf(angle));
	this->x = R.Get_X();
	this->y = R.Get_Y();
}

//指定のベクトルを回転させたベクトルを返す
Vector2D Vector2D::Rotate(Vector2D& vec, float angle){
	Vector2D R;
	R.Set_X(vec.Get_X()*cosf(angle) - vec.Get_Y()*sinf(angle));
	R.Set_Y(vec.Get_X()*sinf(angle) + vec.Get_Y()*cosf(angle));
	return R;
}

//ベクトルの角度を取得
float Vector2D::Get_Angle(){
	return atan2f(this->y, this->x);
}

//ベクトルの長さを取得
float Vector2D::Get_Length(){
	return sqrtf(
		this->x*this->x +
		this->y*this->y);
}

//2つのベクトル(座標)の距離を取得
float Vector2D::Two_Length(Vector2D& v1, Vector2D& v2){
	return sqrtf(
		(v1.Get_X() - v2.Get_X())*(v1.Get_X() - v2.Get_X()) + 
		(v1.Get_Y() - v2.Get_Y())*(v1.Get_Y() - v2.Get_Y()));
}

//加算オペレーター
Vector2D Vector2D::operator+(Vector2D& vec){
	Vector2D R;
	R = *this;
	R.Add(vec);
	return R;

}

//減算オペレーター
Vector2D Vector2D::operator-(Vector2D& vec){
	Vector2D R;
	R = *this;
	R.Sub(vec);
	return R;
}