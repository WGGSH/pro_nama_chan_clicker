class MainGame{
private:
	Game& game;
	std::vector<std::shared_ptr<Target>> target_list;//敵
	std::vector<std::shared_ptr<Weapon>> weapon_list;//武器リスト
	Player *player;//プレイヤー
	Back *back;//背景
	Menu *menu;//メニュー画面

	Vector2D leader_vector[GROUP_MAX];//リーダーのベクトル
	Position2D leader_position[GROUP_MAX];//リーダーの座標

	unsigned int count;//カウンター

	void Update_Target();//敵の更新
	void Update_Player();//プレイヤーの更新

	void Update_Leader();//リーダーベクトルの更新
public:
	MainGame(Game& C_Game);//コンストラクタ
	~MainGame();//デストラクタ
	
	void Initialize();//初期化関数
	void update();//アップデート関数
	
	std::vector<std::shared_ptr<Weapon>>& Get_Weapon_List();//武器リストの取得
	std::vector<std::shared_ptr<Target>>& Get_Target_List();//的リストの取得
	Vector2D* Get_Leader_Velocity();//群れの平均ベクトルを取得
	Position2D* Get_Leader_Position();//群れの中心座標を取得
};