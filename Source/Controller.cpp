#include "main.h"

//コンストラクタ
Controller::Controller(Game& C_Game) :
game(C_Game){

}

//デストラクタ
Controller::~Controller(){

}

//初期関数
void Controller::Initialize(){
	//キーボードの入力状態を初期化
	for (int i = 0; i < 256; i++){
		this->key[i] = 0;
		this->pre_key[i] = 0;
	}

	//マウスの入力状態を初期化する
	this->mouse.x = 0;
	this->mouse.y = 0;
	this->mouse.wheel = 0;
	this->pre_mouse.x = 0;
	this->pre_mouse.y = 0;
	this->pre_mouse.wheel = 0;
	for (int i = 0; i < MOUSE_INPUT_MAX; i++){
		this->mouse.input[i] = 0;
		this->pre_mouse.input[i] = 0;
	}
}

//アップデート関数
void Controller::Update(){
	this->Input_Key();//キーボードの入力を更新
	this->Input_Mouse();//マウスの入力を更新

}

//キーボードの更新関数
//押されていれば連続で押されたフレーム数,
//押されていなければ0
//が保存される
void Controller::Input_Key(){
	char all_key[256];
	GetHitKeyStateAll(all_key);
	for (int i = 0; i < 256; i++){
		this->pre_key[i] = this->key[i];//前フレームの値を記憶
		if (all_key[i] == 1) this->key[i]++;
		else                 this->key[i] = 0;
	}
}

//マウスの更新関数
//マウスの座標,クリックされ続けたフレーム数を記録
//ホイールの回転量は未実装
void Controller::Input_Mouse(){
	//前フレームの座標を保存
	this->pre_mouse = this->mouse;
	//現在の座標を保存
	GetMousePoint(&this->mouse.x, &this->mouse.y);
	for (int i = 0; i < MOUSE_INPUT_MAX; i++){
		if (GetMouseInput()&(MOUSE_INPUT_LEFT+0x0001*i)){
			this->mouse.input[i]++;
		}
		else                  this->mouse.input[i] = 0;
	}
}

//キーの入力状態の取得関数
//指定したキー番号の入力状態を取得
int Controller::Get_Key(int input){
	return this->key[input];
}

//1フレーム前のキーの入力状態を取得
int Controller::Get_Pre_Key(int input){
	return this->pre_key[input];
}

//マウスの入力状態を取得
//Mouse構造体をそのまま返す
Mouse& Controller::Get_Mouse(){
	return this->mouse;
}

//1フレーム前のマウスの入力状態を取得
Mouse& Controller::Get_Pre_Mouse(){
	return this->pre_mouse;
}