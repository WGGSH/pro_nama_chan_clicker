//セーブデータ情報
typedef struct{
	unsigned long version;//バージョン情報
	unsigned long exp;//経験値
	unsigned long play_count;//プレイ時間
	unsigned long target_break;//的を倒した数
	//ここから武器情報
	unsigned int weapon_num_level;//同時発射数のレベル
	unsigned int weapon_angle_level;//発射角度のレベル
	unsigned int weapon_time_level;//存続時間のレベル
	unsigned int target_defence_level;//プロ生ちゃんの耐久力のレベル
	unsigned int target_copy_level;//プロ生ちゃんの増殖レベル
}SaveData;

//オプションデータ情報
typedef struct{
	unsigned int window_expand;//ウィンドウの拡大率
	unsigned int target_draw_max;//的の描画最大数
	unsigned int target_appear_max;//的の出現最大数
}OptionData;

class Save{
private:
	Game& game;
public:
	Save(Game& C_Game);
	~Save();

	void Update();//セーブする
};