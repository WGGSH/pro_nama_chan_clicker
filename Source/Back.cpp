#include "main.h"

//コンストラクタ
Back::Back(Game& C_Game, MainGame& C_MainGame):
game(C_Game), maingame(C_MainGame){

}

//デストラクタ
Back::~Back(){

}

//初期化関数
void Back::Initialize(){
	for (int i = 0; i < 4; i++){
		this->image[i] = this->game.Get_Sprite().Get_Image(IMAGE_BACK01 + i);
	}
	this->count = 300;
}

//アップデート関数
//それぞれの絵を20秒ずつ描画するサイクル
//20秒の初め/終わりの5秒はフェードイン/アウトをする
void Back::Update(){
	this->count++;
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 32);
	int time = this->count % 3600;
	for (int i = 0; i < 4; i++){
		if (time >= 0 + i * 900 && time <= 1200 + i * 900){
			if (time >= 0 + i * 900 && time < 300 + i * 900){
				SetDrawBlendMode(DX_BLENDMODE_ALPHA, (float)(time - i * 900) / 300.0f * 32);
			}
			else if (time >= 900 + i * 900 && time <= 1200 + i * 900){
				SetDrawBlendMode(DX_BLENDMODE_ALPHA, 32 - (float)(time - i * 900 - 900) / 300.0f * 32);
			}
			else{
				SetDrawBlendMode(DX_BLENDMODE_ALPHA, 32);
			}

			DrawGraph(0, 0, this->image[i], TRUE);
		}
		if (i == 3){
			if (time >= 0 && time <= 300){
				SetDrawBlendMode(DX_BLENDMODE_ALPHA, 32 - (float)(time) / 300.0f * 32);
				DrawGraph(0, 0, this->image[i], TRUE);
			}
		}
	}
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
}