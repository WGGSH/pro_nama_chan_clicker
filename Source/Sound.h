class Sound{
private:
	Game& game;
	BOOL sound_handle[SOUND_MAX];//音声再生フラグ
public:
	Sound(Game& C_Game);//コンストラクタ
	~Sound();//デストラクタ

	void Initialize();//初期化関数
	void Update();//更新

	void Set_Sound(int number, BOOL flag);//再生フラグの変更
};