#include "main.h"

//コンストラクタ
Menu::Menu(Game& C_Game, MainGame& C_MainGame,Controller& C_Controller) :
game(C_Game), maingame(C_MainGame),controller(C_Controller){

}

//デストラクタ
Menu::~Menu(){

}

//初期化関数
void Menu::Initialize(){
	this->draw_count = 0;
	this->level_up_select = 0;
	this->hold_flag = FALSE;
}

//キー入力による操作
void Menu::Command(){
	//メニューが表示されていたら
	if (this->draw_count > 140){
		//カーソルの移動
		if (this->controller.Get_Key(KEY_INPUT_UP) == 1){
			this->level_up_select--;
			//カーソル移動効果音
			this->game.Get_Sound().Set_Sound(SOUND_CURSOR_SELECT, TRUE);
		}
		if (this->controller.Get_Key(KEY_INPUT_DOWN) == 1){
			this->level_up_select++;
			//カーソル移動効果音
			this->game.Get_Sound().Set_Sound(SOUND_CURSOR_SELECT, TRUE);
		}
		if (this->level_up_select < 0)this->level_up_select = 4;
		if (this->level_up_select > 4)this->level_up_select = 0;
		//装備のレベルアップ
		if (this->controller.Get_Key(KEY_INPUT_SPACE) == 1){
			switch (this->level_up_select){
			case 0://武器の同時発射数
				//レベルが最大でなければ
				if (this->game.Get_SaveData().weapon_num_level != WEAPON_NUM_LEVEL_MAX - 1){
					//経験値が足りていれば
					if (this->game.Get_SaveData().exp >= WEAPON_NUM_COST[this->game.Get_SaveData().weapon_num_level]){
						this->game.Get_SaveData().exp -= WEAPON_NUM_COST[this->game.Get_SaveData().weapon_num_level];
						this->game.Get_SaveData().weapon_num_level++;

						//レベルアップ効果音
						this->game.Get_Sound().Set_Sound(SOUND_LEVELUP, TRUE);

						//2レベルになったとき,角度が1レベルなら一緒に上げる
						if (this->game.Get_SaveData().weapon_num_level == 1 && this->game.Get_SaveData().weapon_angle_level == 0){
							this->game.Get_SaveData().weapon_angle_level++;
						}
					}
				}
				break;
			case 1://発射角度
				//レベルが最大でなければ
				if (this->game.Get_SaveData().weapon_angle_level != WEAPON_ANGLE_LEVEL_MAX - 1){
					//経験値が足りていれば
					if (this->game.Get_SaveData().exp >= WEAPON_ANGLE_COST[this->game.Get_SaveData().weapon_angle_level]){
						this->game.Get_SaveData().exp -= WEAPON_ANGLE_COST[this->game.Get_SaveData().weapon_angle_level];
						this->game.Get_SaveData().weapon_angle_level++;

						//レベルアップ効果音
						this->game.Get_Sound().Set_Sound(SOUND_LEVELUP, TRUE);

						//2レベルになったとき,同時発射数が1レベルなら一緒に上げる
						if (this->game.Get_SaveData().weapon_angle_level == 1 && this->game.Get_SaveData().weapon_num_level == 0){
							this->game.Get_SaveData().weapon_num_level++;
						}
					}
				}
				break;
			case 2://存続時間
				//レベルが最大でなければ
				if (this->game.Get_SaveData().weapon_time_level != WEAPON_TIME_LEVEL_MAX - 1){
					int use_exp = (this->game.Get_SaveData().weapon_time_level<=35) ?
						(3000 + (int)pow(2.5, (double)this->game.Get_SaveData().weapon_time_level / 2.0)):
						10000000;
					use_exp = 10000 * exp(0.0381*(this->game.Get_SaveData().weapon_time_level * 2.1));
					//経験値が足りていれば
					if (this->game.Get_SaveData().exp >= use_exp){
						this->game.Get_SaveData().exp -= use_exp;
						this->game.Get_SaveData().weapon_time_level++;

						//レベルアップ効果音
						this->game.Get_Sound().Set_Sound(SOUND_LEVELUP, TRUE);

					}
				}
				break;
			case 3://プロ生ちゃん耐久力
				//レベルが最大でなければ
				if (this->game.Get_SaveData().target_defence_level != TARGET_DEFENCE_MAX - 1){
					//経験値が足りていれば
					if (this->game.Get_SaveData().exp >= TARGET_DEFENCE_COST[this->game.Get_SaveData().target_defence_level]){
						this->game.Get_SaveData().exp -= TARGET_DEFENCE_COST[this->game.Get_SaveData().target_defence_level];
						this->game.Get_SaveData().target_defence_level++;

						//レベルアップ効果音
						this->game.Get_Sound().Set_Sound(SOUND_LEVELUP, TRUE);

					}
				}
				break;
			case 4://プロ生ちゃん増殖力
				//レベルが最大でなければ
				if (this->game.Get_SaveData().target_copy_level != TARGET_COPY_LEVEL_MAX - 1){
					//経験値が足りていれば
					if (this->game.Get_SaveData().exp >= TARGET_COPY_COST[this->game.Get_SaveData().target_copy_level]){
						this->game.Get_SaveData().exp -= TARGET_COPY_COST[this->game.Get_SaveData().target_copy_level];
						this->game.Get_SaveData().target_copy_level++;
						//レベルアップ効果音
						this->game.Get_Sound().Set_Sound(SOUND_LEVELUP, TRUE);
					}
				}
				break;
			default://例外処理
				break;
			}
		}
	}

	//セーブデータの保存(オートセーブなのであまり意味はない)
	if (this->controller.Get_Key(KEY_INPUT_S) == 1){
		this->game.Get_Save().Update();
	}
	
	//SSの撮影
	if (this->controller.Get_Key(KEY_INPUT_P) == 1){
		this->game.Set_SS_Flag(TRUE);
	}

	//メニュー画面の固定
	if (this->controller.Get_Key(KEY_INPUT_H) == 1){
		this->hold_flag++;
		this->hold_flag = this->hold_flag % 3;
	}

	//プロ生ちゃん消去処理
	if (this->controller.Get_Key(KEY_INPUT_X) == 1){
		this->maingame.Get_Target_List().clear();
	}
	
	//終了処理
	if (this->controller.Get_Key(KEY_INPUT_Q) == 1){
		this->game.Set_Quit_Flag(TRUE);
	}
}

//描画関数
void Menu::Draw(){
	int draw_x = WINDOW_X - this->draw_count;

	SetDrawArea(draw_x, 0, WINDOW_X, WINDOW_Y);
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 224);
	DrawBox(draw_x, 0, WINDOW_X, WINDOW_Y, 0x000000, TRUE);
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);


	//プレイ時間の描画
	int hour = this->game.Get_SaveData().play_count / 216000;
	int min = (this->game.Get_SaveData().play_count / 3600) % 60;
	int sec = (this->game.Get_SaveData().play_count / 60) % 60;
	if (min < 10 && sec < 10){
		DrawFormatString(draw_x, 10, 0xffffff, "プレイ時間 %d:0%d:0%d",
			hour, min, sec);
	}
	else if (min >= 10 && sec <= 10){
		DrawFormatString(draw_x, 10, 0xffffff, "プレイ時間 %d:%d:0%d",
			hour, min, sec);
	}
	else if (min<10 && sec >= 10){
		DrawFormatString(draw_x, 10, 0xffffff, "プレイ時間 %d:0%d:%d",
			hour, min, sec);
	}
	else{
		DrawFormatString(draw_x, 10, 0xffffff, "プレイ時間 %d:%d:%d", 
			hour, min, sec);
	}

	//退治数の描画
	DrawFormatString(draw_x, 30, 0xffffff, "退治した数:%d匹", this->game.Get_SaveData().target_break);

	//経験値の描画
	DrawFormatString(draw_x, 50, 0xffffff, "%dExp", this->game.Get_SaveData().exp);

	//各種レベルの描画
	DrawFormatString(draw_x, 80, 0xffffff, "同時発射数　レベル%d", this->game.Get_SaveData().weapon_num_level+1);
	DrawFormatString(draw_x, 130, 0xffffff, "武器発射角度　レベル%d", this->game.Get_SaveData().weapon_angle_level+1);
	DrawFormatString(draw_x, 180, 0xffffff, "武器存続時間　レベル%d", this->game.Get_SaveData().weapon_time_level+1);
	DrawFormatString(draw_x, 230, 0xffffff, "プロ生ちゃん耐久力 レベル%d", this->game.Get_SaveData().target_defence_level + 1);
	DrawFormatString(draw_x, 280, 0xffffff, "プロ生ちゃん増殖力 レベル%d", this->game.Get_SaveData().target_copy_level + 1);

	//レベルアップコマンドの描画
	if (this->game.Get_SaveData().weapon_num_level != WEAPON_NUM_LEVEL_MAX - 1){
		DrawFormatString(draw_x, 100, 0xffffff, "LEVEL UP！！　%d→%d:%dExp",
			this->game.Get_SaveData().weapon_num_level + 1, this->game.Get_SaveData().weapon_num_level + 2, WEAPON_NUM_COST[this->game.Get_SaveData().weapon_num_level]);
	}
	else{
		DrawFormatString(draw_x, 100, 0xffffff, "LEVEL MAX！！　%d",
			WEAPON_NUM_LEVEL_MAX);
	}
	if (this->game.Get_SaveData().weapon_angle_level != WEAPON_ANGLE_LEVEL_MAX - 1){
		DrawFormatString(draw_x, 150, 0xffffff, "LEVEL UP！！  %d→%d:%dExp",
			this->game.Get_SaveData().weapon_angle_level + 1, this->game.Get_SaveData().weapon_angle_level + 2, WEAPON_ANGLE_COST[this->game.Get_SaveData().weapon_angle_level]);
	}
	else{
		DrawFormatString(draw_x, 150, 0xffffff, "LEVEL MAX！！  %d",
			WEAPON_ANGLE_LEVEL_MAX);
	}
	if (this->game.Get_SaveData().weapon_time_level != WEAPON_TIME_LEVEL_MAX - 1){
		int use_exp = 10000 * exp(0.0381*(this->game.Get_SaveData().weapon_time_level * 2.1));
		DrawFormatString(draw_x, 200, 0xffffff, "LEVEL UP！！  %d→%d:%dExp",
			this->game.Get_SaveData().weapon_time_level + 1, this->game.Get_SaveData().weapon_time_level + 2, use_exp);
	}
	else{
		DrawFormatString(draw_x, 200, 0xffffff, "LEVEL MAX！！  %d",
			WEAPON_TIME_LEVEL_MAX);
	}
	if (this->game.Get_SaveData().target_defence_level != TARGET_DEFENCE_MAX - 1){
		DrawFormatString(draw_x, 250, 0xffffff, "LEVEL UP！！  %d→%d:%dExp",
			this->game.Get_SaveData().target_defence_level + 1, this->game.Get_SaveData().target_defence_level + 2, TARGET_DEFENCE_COST[this->game.Get_SaveData().target_defence_level]);
	}
	else{
		DrawFormatString(draw_x, 250, 0xffffff, "LEVEL MAX！！ %d",
			TARGET_DEFENCE_MAX);
	}
	if (this->game.Get_SaveData().target_copy_level != TARGET_COPY_LEVEL_MAX - 1){
		DrawFormatString(draw_x, 300, 0xffffff, "LEVEL UP！！  %d→%d:%dExp",
			this->game.Get_SaveData().target_copy_level + 1, this->game.Get_SaveData().target_copy_level + 2, TARGET_COPY_COST[this->game.Get_SaveData().target_copy_level]);
	}
	else{
		DrawFormatString(draw_x, 300,0xffffff, "LEVEL MAX！！  %d",
			TARGET_COPY_LEVEL_MAX);
	}
	DrawBox(draw_x, 98 + this->level_up_select * 50, draw_x + 275, 118 + this->level_up_select * 50, 0x0000ff, FALSE);
	DrawFormatString(draw_x,320, 0xffffff, "↑↓キーで選択　Spaceキーで実行");

	//各種実行コマンドの描画
	DrawFormatString(draw_x, 360, 0xffffff, "S:セーブデータ保存");
	DrawFormatString(draw_x, 380, 0xffffff, "H:メニュー画面切り替え");
	DrawFormatString(draw_x, 400, 0xffffff, "E:エフェクトの表示/非表示");
	DrawFormatString(draw_x, 420, 0xffffff, "P:画面撮影");
	DrawFormatString(draw_x, 440, 0xffffff, "X:プロ生ちゃんを消す");
	DrawFormatString(draw_x, 460, 0xffffff, "Q:終了");

	//プロ生ちゃんの数の描画
	DrawFormatString(draw_x, 500, 0xffffff, "画面上のプロ生ちゃん %d匹", this->maingame.Get_Target_List().size());
	


	SetDrawArea(0, 0, WINDOW_X, WINDOW_Y);
}

//アップデート関数
void Menu::Update(){
	//メニューバーの描画
	switch (this->hold_flag){
	case 0://スライド方式
		if (this->controller.Get_Mouse().x > 1000)this->draw_count += 15;
		else this->draw_count -= 30;
		if (this->draw_count > 280)this->draw_count = 280;
		if (this->draw_count < 0)this->draw_count = 0;
		break;
	case 1://常に表示
		this->draw_count = 280;
		break;
	case 2://常に非表示
		this->draw_count = 0;
		break;
	default:
		break;
	}
	

	this->Command();//キー入力判定
	this->Draw();//描画
}