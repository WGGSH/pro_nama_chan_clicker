class Sprite{
private:
	Game& game;
	int image[IMAGE_MAX];//画像データ
	int sound[SOUND_MAX];//音声データ
public:
	Sprite(Game& C_Game);//コンストラクタ
	~Sprite();//デストラクタ

	void Initialize();//初期化関数
	int& Get_Image(int number);//画像の取得
	int& Get_Sound(int number);//音声の取得
};