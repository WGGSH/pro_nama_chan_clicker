#include "main.h"

//コンストラクタ
Weapon::Weapon(Game& C_Game, MainGame& C_MainGame,Position2D& pos, float speed, float angle,int count) :
game(C_Game), maingame(C_MainGame){
	//初期化
	this->position = pos;
	this->velocity.Set_Angle(speed, angle);
	this->angle = angle;
	this->count = count;
}

//デストラクタ
Weapon::~Weapon(){

}

//座標の取得
Position2D& Weapon::Get_Position(){
	return this->position;
}

//描画関数
void Weapon::Draw(){
	SetDrawBlendMode(DX_BLENDMODE_ADD, 192);
	DrawRotaGraphF(
		this->position.Get_X(), this->position.Get_Y(),
		1.0f, this->angle-PI/2,
		this->game.Get_Sprite().Get_Image(IMAGE_WEAPON), TRUE);
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
}

//アップデート関数
BOOL Weapon::Update(){
	this->count--;//カウンターを減算
	//座標の加算
	this->position.Add(this->velocity);

	//画面外に出ると反射
	if (this->position.Get_X() < 0 || this->position.Get_X() > WINDOW_X){
		this->velocity.Set_X(this->velocity.Get_X()*(-1));
		this->angle = this->velocity.Get_Angle();
	}
	if (this->position.Get_Y() < 0 || this->position.Get_Y() > WINDOW_Y){
		this->velocity.Set_Y(this->velocity.Get_Y()*(-1));
		this->angle = this->velocity.Get_Angle();
	}
	//描画
	this->Draw();

	if (this->count == 0)return FALSE;//カウンターが0になると消滅

	return TRUE;
}