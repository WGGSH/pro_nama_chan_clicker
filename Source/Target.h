class Target{
private:
	Game& game;
	MainGame& maingame;
	Position2D position;//座標
	Vector2D velocity;//移動量
	float size_rate;//拡大率
	float angle;//向き
	int state;//状態
	//0:登場直後無敵時間
	//1:通常時
	//2:被弾時1
	//3:被弾時2
	int count;//カウンタ
	int group;//所属番号

	void Collision();//当たり判定
	void Move();//移動制御
public:
	Target(Game& C_Game, MainGame& C_Maingame,Position2D pos,float size);//コンストラクタ
	~Target();//デストラクタ

	void initialize();//初期化関数
	BOOL Update();//アップデート関数
	void Draw();//描画関数

	Position2D& Get_Position();//座標の取得
	Vector2D& Get_Vector();//ベクトルの取得
	float& Get_Size_Rate();//大きさの取得
	int Get_Group();//所属番号の取得

};