//マウス構造体
typedef struct{
	int x;//X座標
	int y;//Y座標
	int wheel;//ホイールの回転量
	int input[MOUSE_INPUT_MAX];//ボタン判定
}Mouse;

class Controller{
private:
	Game& game;
	int key[256];//キー入力情報
	int pre_key[256];//1フレーム前のキー入力情報
	Mouse mouse;//マウス入力情報
	Mouse pre_mouse;//1フレーム前のマウス入力情報

	void Input_Key();//キー情報を登録
	void Input_Mouse();//マウス情報を登録
public:
	Controller(Game& C_Game);//コンストラクタ
	~Controller();//デストラクタ

	void Initialize();//初期化関数
	void Update();//アップデート関数

	int Get_Key(int input);//キーの状態を渡す
	int Get_Pre_Key(int input);//前フレームのキー状態を渡す
	Mouse& Get_Mouse();//マウスの状態を渡す
	Mouse& Get_Pre_Mouse();//前フレームのマウス状態を渡す
};

