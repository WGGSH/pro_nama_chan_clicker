class Game{
private:
	Sprite *sprite;
	Controller *controller;
	Save *save;
	Load *load;
	Sound *sound;
	Fps *fps;

	MainGame *maingame;

	SaveData *savedata;
	OptionData *optiondata;

	BOOL ss_flag;//SSを撮影するかどうか
	BOOL quit_flag;//ゲームを終了するか
public:
	Game();//コンストラクタ
	~Game();//デストラクタ

	void Initialize();//初期化関数
	void Update();//更新関数

	MainGame& Get_MainGame();
	Controller& Get_Controller();
	Sprite& Get_Sprite();
	Save& Get_Save();
	Load& Get_Load();
	SaveData& Get_SaveData();
	OptionData& Get_OptionData();
	Sound& Get_Sound();
	
	void Set_SS_Flag(BOOL flag);
	void Set_Quit_Flag(BOOL flag);
};