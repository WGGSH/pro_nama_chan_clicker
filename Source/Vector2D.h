//2次元ベクトルクラス
class Vector2D{
private:
	float x;//X成分
	float y;//Y成分
public:
	Vector2D();//コンストラクタ1
	Vector2D(float x, float y);//コンストラクタ2
	~Vector2D();//デストラクタ

	void Set(float x, float y);//値の代入1
	void Set_Angle(float length, float angle);//2値の代入2
	void Set_X(float value);//X成分代入
	void Set_Y(float value);//Y成分代入
	float Get_X();//X成分取得
	float Get_Y();//Y成分取得
	float Get_Angle();//角度の取得
	float Get_Length();//長さの取得

	void Add(Vector2D& vec);//ベクトルを引数に取る加算関数
	void Add(float x, float y);//2実数を引数に取る加算関数
	void Sub(Vector2D& vec);//ベクトルを引数に取る減算関数
	void Sub(float x, float y);//2実数を引数に取る減算関数
	void Mul(float mul);//実数を引数に取る乗算関数
	void Div(float div);//実数を引数に取る除算関数
	void Norm();//正規化
	void Rotate(float angle);//ベクトルの回転
	Vector2D Rotate(Vector2D& vec,float angle);//指定したベクトルを回転させたベクトルを返す
	
	static float Two_Length(Vector2D& v1, Vector2D& v2);//2つのベクトルの距離を取得

	Vector2D operator+(Vector2D& vec);//加算オペレーター
	Vector2D operator-(Vector2D& vec);//減算オペレーター
	
	
};

typedef Vector2D Position2D;//Position2DをVector2Dとしても扱う