#include "main.h"

//コンストラクタ
MainGame::MainGame(Game& C_Game) :
game(C_Game){
	this->player = new Player(this->game, *this, this->game.Get_Controller());
	this->back = new Back(this->game,*this);
	this->menu = new Menu(this->game, *this,this->game.Get_Controller());
}

//デストラクタ
MainGame::~MainGame(){

}

//武器リストの取得
std::vector<std::shared_ptr<Weapon>>& MainGame::Get_Weapon_List(){
	return this->weapon_list;
}

//的リストの取得
std::vector<std::shared_ptr<Target>>& MainGame::Get_Target_List(){
	return this->target_list;
}

//リーダーベクトルの取得
Vector2D* MainGame::Get_Leader_Velocity(){
	return this->leader_vector;
}

//リーダー座標の取得
Position2D* MainGame::Get_Leader_Position(){
	return this->leader_position;
}

//初期化関数
void MainGame::Initialize(){
	//リストの初期化
	this->target_list.clear();

	//初期配置
	for (int i = 0; i < 2; i++){
		Position2D pos((float)GetRand(FIELD_X), (float)GetRand(FIELD_Y));
		this->target_list.push_back(std::shared_ptr<Target>(new Target(
			this->game,*this,pos,100.0f)));
	}
	//武器リストの初期化
	this->weapon_list.clear();

	//各クラスの初期化
	this->player->Inialize();
	this->back->Initialize();
	this->menu->Initialize();

	//カウンタの初期化
	this->count = 0;

}

//リーダーベクトルの更新関数
void MainGame::Update_Leader(){
	//偶に集団の意志を破棄する
	if (GetRand(3000) == 0){
		//if (this->game.Get_Controller().Get_Key(KEY_INPUT_U)==1){
		for (auto Tar : this->target_list){
			Tar.get()->Get_Vector().Set_Angle(5.0f, PI / 180 * GetRand(360));
		}
	}

	Vector2D vec[GROUP_MAX];
	Position2D pos[GROUP_MAX];
	int size[GROUP_MAX] = { 0 };
	for (int i = 0; i < GROUP_MAX; i++){
		vec[i].Set(0, 0);
		pos[i].Set(0, 0);
	}
	for (auto Tar : this->target_list){
		vec[Tar.get()->Get_Group()].Add(Tar.get()->Get_Vector());
		pos[Tar.get()->Get_Group()].Add(Tar.get()->Get_Position());
		size[Tar.get()->Get_Group()]++;
	}
	for (int i = 0; i < GROUP_MAX; i++){
		vec[i].Div(size[i]);
		pos[i].Div(size[i]);
		this->leader_vector[i] = vec[i];//群れ全体のベクトルの平均
		this->leader_position[i] = pos[i];//群れの中心座標
	}
}

//敵更新関数
void MainGame::Update_Target(){
	int count = 0;//同フレーム内で倒したプロ生ちゃんの数
	int draw_count = 0;//描画回数
	std::vector<Position2D> list_position;
	std::vector<float> list_size;
	for (std::vector<std::shared_ptr<Target>>::iterator Ta = this->target_list.begin();
		Ta != this->target_list.end();){
		if (Ta->get()->Update() == FALSE){
			//倒した場合
			count++;
			this->game.Get_SaveData().target_break++;//倒した的の数の加算
			this->game.Get_SaveData().exp += 3;//経験値の加算
			list_position.push_back(Ta->get()->Get_Position());
			list_size.push_back(Ta->get()->Get_Size_Rate());
			//消去
			Ta = this->target_list.erase(Ta);
			continue;
		}
		else{
			//倒していなければ
			draw_count++;
			if (draw_count <= this->game.Get_OptionData().target_draw_max){
				//描画回数が上限に達していなければ
				Ta->get()->Draw();//描画
			}
		}
		++Ta;//ループカウンタの加算
	}

	//同時に倒せばボーナスゲット
	if (count >= 2){
		this->game.Get_SaveData().exp += (int)(log10(count)*20.0);
	}
	//追加分
	//倒した数だけループする
	for (int i = 0; i < count; i++){
		//サイズが下限を切ると増殖しない
		if (list_size[i] < TARGET_DEFENCE[this->game.Get_SaveData().target_defence_level])continue;
		//出現上限を上回るとループ脱出
		if (this->target_list.size() > this->game.Get_OptionData().target_appear_max)break;
		for (int j = 0; j < TARGET_COPY_NUM[this->game.Get_SaveData().target_copy_level]; j++){
			this->target_list.push_back(std::shared_ptr<Target>(new Target(
				this->game, *this, list_position[i], list_size[i] * 0.9f)));
		}
	}
	//一定時間(20秒)ごとに追加
	//またはプロ生ちゃんが1匹もいなくなれば追加
	//出現上限に捉われない
	if (this->count % 1200 == 0 || this->target_list.size()==0){
		for (int i = 0; i < this->game.Get_SaveData().target_defence_level + 1; i++){
			Position2D pos((float)GetRand(FIELD_X), (float)GetRand(FIELD_Y));
			this->target_list.push_back(std::shared_ptr<Target>(new Target(
				this->game, *this, pos, 100.0f)));
		}
	}
}

//プレイヤー更新関数
void MainGame::Update_Player(){
	//武器のループ
	for (std::vector<std::shared_ptr<Weapon>>::iterator itWeapon = this->weapon_list.begin();
		itWeapon != this->weapon_list.end();){
		if (itWeapon->get()->Update() == FALSE){
			itWeapon = this->weapon_list.erase(itWeapon);
			continue;
		}
		++itWeapon;
	}
	//プレイヤーの更新
	this->player->UpDate();
}

//アップデート関数
void MainGame::update(){
	this->count++;//カウンタ加算
	this->game.Get_SaveData().play_count += 1;//プレイ時間の加算
	//部屋の描画
	DrawGraph(0, 0, this->game.Get_Sprite().Get_Image(IMAGE_ROOM),TRUE);

	this->Update_Leader();//群れ情報の更新

	this->Update_Target();//プロ生ちゃんの更新
	this->Update_Player();//プレイヤーの更新
	this->menu->Update();//メニュー画面の更新

	this->back->Update();//背景の更新

	//5秒おきに自動セーブ
	if (this->count % 300 == 0)this->game.Get_Save().Update();
}
