//メニュークラス
//ステータスの上昇やプレイログなどが見られる
//カーソルを画面左に持っていくとみることができるようになる

class Menu{
private:
	Game& game;
	MainGame& maingame;
	Controller& controller;

	int draw_count;//メニューの表示幅
	int level_up_select;//レベルアップ選択番号
	BOOL hold_flag;//メニュー画面を固定化するか

	void Command();//キー入力による操作
	void Draw();
public:
	Menu(Game& C_Game, MainGame& C_MainGame,Controller& C_Controller);
	~Menu();
	
	void Initialize();
	void Update();
};